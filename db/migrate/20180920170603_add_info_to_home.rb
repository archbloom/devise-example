class AddInfoToHome < ActiveRecord::Migration[5.0]
  def change
    add_column :homes, :name, :string
    add_column :homes, :owner, :string
  end
end
